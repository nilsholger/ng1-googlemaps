var app = angular.module('nggoogleMAps', ['addCtrl', 'queryCtrl', 'geolocation', 'gservice', 'ngRoute'])
          .config(function($routeProvider){

            $routeProvider.when('/join', {
              controller: 'addCtrl',
              templateUrl: 'partials/addForm.html'
            }).when('/find', {
              controller: 'queryCtrl',
              templateUrl: 'partials/queryForm.html'
            }).when('/about', {
              templateUrl: 'partials/aboutForm.html'
            }).otherwise({ redirectTo: '/join'})
          });
