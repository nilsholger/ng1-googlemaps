var queryCtrl = angular.module('queryCtrl', ['geolocation', 'gservice']);

queryCtrl.controller('queryCtrl', function($scope, $log, $http, $rootScope, geolocation, gservice){


        $scope.formData = {};
        var queryBody = {};

        geolocation.getLocation().then(function(data){
                coords = {lat: data.coords.latitude, long: data.coords.longitude};
                $scope.formData.longitude = parseFloat(coords.long).toFixed(3);
                $scope.formData.latitude = parseFloat(coords.lat).toFixed(3);
        });

        $rootScope.$on("clicked", function() {

        $scope.$apply(function(){
          $scope.formData.latitude = parseFloat(gservice.clickLat).toFixed(3);
          $scope.formData.longitude = parseFloat(gservice.clickLong).toFixed(3);
        });
    });

      $scope.queryUsers = function() {
            queryBody = {
                longitude: parseFloat($scope.formData.longitude),
                latitude: parseFloat($scope.formData.latitude),
                distance: parseFloat($scope.formData.distance),
                male: $scope.formData.male,
                female: $scope.formData.female,
                android: $scope.formData.android,
                minAge: $scope.formData.minage,
                maxAge: $scope.formData.maxAge,
                favlang: $scope.formData.favLang,
                reqVerified: $scope.formData.verified
            };

            $http.post('/query', queryBody)

                .success(function(queryResults){

                  console.log("query body:");
                  console.log(queryBody);
                  console.log("query results:");
                  console.log(queryResults);
                  $scope.queryCount = queryResults.length;
                })
                .error(function(queryResults) {
                  console.log('error ' + queryResults);
                })
      };
  });
